package io.nats.examples;

import io.nats.client.AuthHandler;
import io.nats.client.Connection;
import io.nats.client.NKey;
import io.nats.client.Nats;
import io.nats.client.Options;

import javax.net.ssl.*;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.Security;

// [begin connect_tls]
class SSLUtils {
	
    public static String KEYSTORE_PATH = "src/main/resources/keystore.jks";
    public static String TRUSTSTORE_PATH = "src/main/resources/truststore.jks";
    
    public static String STORE_PASSWORD = "7ajshs90s";
    public static String KEY_PASSWORD = "7ajshs90s";
    
    public static String ALGORITHM = "SunX509";

    public static KeyStore loadKeystore(String path) throws Exception {
        KeyStore store = KeyStore.getInstance("BKS", "BC");
        BufferedInputStream in = new BufferedInputStream(new FileInputStream(path));

        try {
            store.load(in, STORE_PASSWORD.toCharArray());
        } finally {
            if (in != null) {
                in.close();
            }
        }

        return store;
    }

    public static KeyManager[] createTestKeyManagers() throws Exception {
        KeyStore store = loadKeystore(KEYSTORE_PATH);
        KeyManagerFactory factory = KeyManagerFactory.getInstance(ALGORITHM);
        factory.init(store, KEY_PASSWORD.toCharArray());
        return factory.getKeyManagers();
    }

    public static TrustManager[] createTestTrustManagers() throws Exception {
        KeyStore store = loadKeystore(TRUSTSTORE_PATH);
        TrustManagerFactory factory = TrustManagerFactory.getInstance(ALGORITHM);
        factory.init(store);
        return factory.getTrustManagers();
    }

    public static SSLContext createSSLContext() throws Exception {
        SSLContext ctx = SSLContext.getInstance(Options.DEFAULT_SSL_PROTOCOL);
        ctx.init(createTestKeyManagers(), createTestTrustManagers(), new SecureRandom());
        return ctx;
    }
}

public class TLSConnector {
	
	/**
	 * This values must have been defined asomewhere on NATS server.
	 * They are JWT Token and NKEY Seed
	 * 
	 * Jwt:  "eyJ0eXAiOiJKV1QiLCJhbGciOiJlZDI1NTE5LW5rZXkifQ.eyJqdGkiOiJRTkhWWUVPUUhMRkRDWDJOVzNQSURGWU9BWjdCQlM0S0w0WkRCQVZRQ1ZSNFlET1JBVkpBIiwiaWF0IjoxNjMxNzc3MTYyLCJpc3MiOiJBRFlGTUpLRlJHN0E2V1JWUkVRWUVZTEczSkVKRUFJM1M3TzRBQVJLTzVPNEU1TDVVNlMyNkpBTiIsIm5hbWUiOiJlbGx5cG9zLWJyaWRnZS1zdGFnaW5nLXBvcyIsInN1YiI6IlVCWlpBNE9KTEFaUlBOWkoyWUQ0UVdVSUtMMzQ2NTRUT1RZNkNCRFI0N0pJN0RXRkJYNVhUUE1IIiwibmF0cyI6eyJwdWIiOnt9LCJzdWIiOnt9LCJzdWJzIjotMSwiZGF0YSI6LTEsInBheWxvYWQiOi0xLCJ0eXBlIjoidXNlciIsInZlcnNpb24iOjJ9fQ.60qtnWXBzhl_DUOH1V1ru8FwmDgljieJwwfV4B879UxGbOF0GvRjdtw3fN78VFA9zs8pTpmPwazMovPzXbmFDQ",
	 * Nkey: "SUAJHNX7UWRSFI4Z3RG7JPNT2ZEWDD3FFQKSN3ND52BIXI7EBAJPIBMRH4",
	 * 
	 */
    public static void main(String[] args) {
    	
    	NKey theNKey = NKey.fromSeed("SUAJHNX7UWRSFI4Z3RG7JPNT2ZEWDD3FFQKSN3ND52BIXI7EBAJPIBMRH4".toCharArray());
    	
        try {
        	Security.addProvider(new BouncyCastleProvider());
            SSLContext ctx = SSLUtils.createSSLContext();
            Options options = new Options.Builder()
					.server("nats://nats.staging.ellypos.io:80")
                    .sslContext(ctx) // Set the SSL context
                    .authHandler(new AuthHandler(){
                        public char[] getID() {
                            try {
                                return theNKey.getPublicKey();
                            } catch (GeneralSecurityException|IOException|NullPointerException ex) {
                                return null;
                            }
                        }

                        public byte[] sign(byte[] nonce) {
                            try {
                                return theNKey.sign(nonce);
                            } catch (GeneralSecurityException|IOException|NullPointerException ex) {
                                return null;
                            }
                        }

                        public char[] getJWT() {
                            return "eyJ0eXAiOiJKV1QiLCJhbGciOiJlZDI1NTE5LW5rZXkifQ.eyJqdGkiOiJRTkhWWUVPUUhMRkRDWDJOVzNQSURGWU9BWjdCQlM0S0w0WkRCQVZRQ1ZSNFlET1JBVkpBIiwiaWF0IjoxNjMxNzc3MTYyLCJpc3MiOiJBRFlGTUpLRlJHN0E2V1JWUkVRWUVZTEczSkVKRUFJM1M3TzRBQVJLTzVPNEU1TDVVNlMyNkpBTiIsIm5hbWUiOiJlbGx5cG9zLWJyaWRnZS1zdGFnaW5nLXBvcyIsInN1YiI6IlVCWlpBNE9KTEFaUlBOWkoyWUQ0UVdVSUtMMzQ2NTRUT1RZNkNCRFI0N0pJN0RXRkJYNVhUUE1IIiwibmF0cyI6eyJwdWIiOnt9LCJzdWIiOnt9LCJzdWJzIjotMSwiZGF0YSI6LTEsInBheWxvYWQiOi0xLCJ0eXBlIjoidXNlciIsInZlcnNpb24iOjJ9fQ.60qtnWXBzhl_DUOH1V1ru8FwmDgljieJwwfV4B879UxGbOF0GvRjdtw3fN78VFA9zs8pTpmPwazMovPzXbmFDQ".toCharArray();
                        }
                    })
                    .build();
            
            try (Connection nc = Nats.connect(options)) {
            	System.out.println("Client: " + Nats.CLIENT_VERSION);
                System.out.println("Server: " + nc.getServerInfo().getVersion());
            }
            catch (InterruptedException e) {
            	e.printStackTrace(System.err);
            }
            catch (IOException e) {
            	e.printStackTrace(System.err);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
// [end connect_tls]