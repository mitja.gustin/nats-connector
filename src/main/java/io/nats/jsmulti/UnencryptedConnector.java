package io.nats.jsmulti;

import io.nats.client.Connection;
import io.nats.client.Nats;

import java.io.IOException;

/**
 * 
 * Tester with no SSL
 * <a href="https://github.com/nats-io/java-nats-examples">Github</a>
 * Location: /home/mgustin/codewsl/nats 
 * @author mgustin
 *
 */
public class UnencryptedConnector
{
    public static void main1( String[] args )
    {
    	
    	// nats.staging.ellypos.io:80
    	
        try (Connection nc = Nats.connect("nats://nats.staging.ellypos.io:80")) {
            
        	System.out.println("Client: " + Nats.CLIENT_VERSION);
            System.out.println("Server: " + nc.getServerInfo().getVersion());
            
        }
        catch (InterruptedException e) {
        	e.printStackTrace(System.err);
        }
        catch (IOException e) {
        	e.printStackTrace(System.err);
        }
    }
}
